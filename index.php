<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Ingenieria Economica</title>
	<link rel="icon" type="image/png" href="favicon.ico" />

	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img width="40%" src="img/logoeconommica.png" alt=""></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">

			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-calculator"></i> Calculador de Credito</h3>
			</div>
			<div class="panel-body">
				<form name="formCredit">
					  <div class="form-group col-md-4">
					    <label for="exampleInputEmail1">Nombre</label>
					    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" 	 data-parsley-errors-messages-disabled="" data-parsley-required="true" >
					  </div>
					  <div class="form-group col-md-4">
					    <label for="cedula">Cedula</label>
					    <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cedula" 	 data-parsley-errors-messages-disabled="" data-parsley-required="true" >
					  </div>
					   <div class="form-group col-md-4">
					    <label for="exampleInputPassword1">Valor Prestamo</label>
					    <input type="text" name="prestamo" class="form-control number-format" id="prestamo" placeholder="Valor Prestamo" 	 data-parsley-errors-messages-disabled="" data-parsley-required="true" autocomplete="off">
					  </div>
					  <div class="form-group col-md-4">
					    <label for="plazo">Numero de Años</label>
					    <select name="plazo" id="plazo" class="form-control" >
					    	<option value="0">Seleccione</option>
					    	<option value="2">2</option>
					    	<option value="3">3</option>
					    	<option value="4">4</option>
					    	<option value="5">5</option>
					    	<option value="6">6</option>
					    	<option value="7">7</option>
					    </select>
					  </div>
					  <div class="form-group col-md-4">
					    	<div class="checkbox col-md-3">
					  			<label>
					  				<input class="checkbox-per" name="periodo" type="radio" value="12">
					  				Mensual
					  			</label>
					  		</div>
					  		<div class="checkbox col-md-3">
					  			<label>
					  				<input class="checkbox-per" name="periodo" type="radio" value="6">
					  				Bimestral
					  			</label>
					  		</div>
					  		<div class="checkbox col-md-3">
					  			<label>
					  				<input class="checkbox-per" name="periodo" type="radio" value="4">
					  				Trimestral
					  			</label>
					  		</div>
					  </div>
					  <div class="form-group col-md-4">
					  	<div class="col-md-4">
					  	<label for="valinte">Efectiva Anual</label>
					  		<input type="text" name="eanual" id="eanual" class="form-control interes" data-idInt="1" value=""   data-parsley-errors-messages-disabled data-parsley-required="true">
					  		<input type="hidden" name="eanualh" id="eanualh">
					  	</div>
					  	<div class="col-md-4">
					  	<label for="valinte">Nominal Anual</label>
					  		<input type="text" name="nanual" id="nanual" class="form-control interes" data-idInt="2" value=""  	 data-parsley-errors-messages-disabled data-parsley-required="true" >
					  		<input type="hidden" name="nanualh" id="nanualh">
					  	</div>
					  	<div class="col-md-4">
					  	<label for="valinte">Interes Periodico</label>
					  		<input type="text" name="iperiodico" id="iperiodico" class="form-control interes" data-idInt="3" value=""  	data-parsley-errors-messages-disabled data-parsley-required="true"  >
					  		<input type="hidden" name="iperiodicoh" id="iperiodicoh">
					  	</div>
					  </div>
					  <button type="submit" class="btn btn-success btn-block">Enviar</button>
					</form>
			</div>
		</div>
		<!-- Ini Boton -->
		<button type="button" class="btn btn-info btn-paralelo" style="display: none;" data-toggle="modal" data-target="#modalparalelo"><i class="fa fa-object-group"></i> Ver Paralelo</button>
		<!-- fin boton -->
		<button type="button" class="btn btn-danger btn-pdf"  style="display: none;"><i class="fa fa-file"></i> Generar PDF</button><br><br>


		<div class="panel panel-primary panel-header panel-header-1" data-close="0">
			<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-calculator"></i> Tabla Interes Capital</h3>
			</div>
			<div class="panel-body panel-slide">
			<div class="col-md-12">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Prestamo</th>
							<th>Capital</th>
							<th>Interes</th>
							<th>Flujo de Caja</th>
						</tr>
					</thead>
					<tbody class="capital">
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
			</div>
        </div>
	<div class="panel panel-primary panel-header panel-header-2" data-close="0">
		<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-calculator"></i> Tabla Interes Amortizacion Gradual</h3>
		</div>
		<div class="panel-body panel-slide">
		<div class="col-md-12">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Fecha</th>
					<th>Saldo Capital</th>
					<th>Amortizacion capital</th>
					<th>Amortizacion interes</th>
					<th>Cuota fija</th>
					<th>Flujo de caja</th>

				</tr>
			</thead>
			<tbody class="amortizacion">
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		</div>
	</div>
		</div>


	</div>
</body>
	<script src="js/jquery-3.2.1.js"></script>
	<script src="bootstrap/js/bootstrap.js"></script>
	<script src="js/parsley/parsley.js"></script>
	<script src="js/jquery.number.js"></script>
	<script src="js/html2canvas.min.js"></script>
	<script src="js/funciones.js"></script>
	<script>
		$(document).ready(function() {
			validFormParsley();
			calculateInteres();
			inputNumerico('#cedula');
			limit_char("#cedula", 15);
			$(".number-format").number(true, 0);
			validateDecimals(".interes");
			function validFormParsley() {
			    var formValid = $("form[name=formCredit]");
			    validateParsley(formValid);
			    formValid.parsley();
			    formValid.on('submit', function(e) {
			        e.preventDefault();
			        dataIn=formValid.serialize();
			        if ($(this).parsley().validate() === true) {
			          $.ajax({
			          	url: 'controllers/cuotas.php?data=1',
			          	type: 'POST',
			          	dataType: 'json',
			          	data: dataIn,
			          	success:function(data){

			          		$(".capital").html(data.data);
			          	}


			          });
			          $.ajax({
			          	url: 'controllers/cuotas.php?data=2',
			          	type: 'POST',
			          	dataType: 'json',
			          	data: dataIn,
			          	success:function(datas){
			          		$(".amortizacion").html(datas.data);
			          		$(".btn-paralelo,.btn-pdf").show();

			          	}


			          });
			          $(".panel-header").find(".panel-slide").slideDown();
			          $(".panel-header").attr("data-close",1);

			        } else {
			            alert("");
			        }
			    });
			}
			function calculateInteres()
			{
				$(".interes").on("focusout",function(){
					idInt = $(this).attr("data-idInt");
					var formValid = $("form[name=formCredit]");
					data = formValid.serialize();
					if($(this).val()>0 && $(this).val()!="" && $(".plazo").val() !="" && $("input:checked").length > 0)
					{

						$.ajax({
							url: 'controllers/interes.php?data='+idInt,
							type: 'POST',
							dataType: 'json',
							data: data,
							success:function(data){
								switch (data.method) {
									case 1:
										 $("#nanual").val(data.interes2);
										 $("#iperiodico").val(data.interes3);
										 $("#eanualh").val(data.interesh1);
										 $("#nanualh").val(data.interesh2);
										 $("#iperiodicoh").val(data.interesh3);
									break;
									case 2:
										 $("#eanual").val(data.interes1);
										 $("#iperiodico").val(data.interes3);
										 $("#eanualh").val(data.interesh1)
										 $("#nanualh").val(data.interesh2)
										 $("#iperiodicoh").val(data.interesh3)
									break;
									case 3:
										 $("#eanual").val(data.interes1);
										 $("#nanual").val(data.interes2);
										 $("#eanualh").val(data.interesh1)
										 $("#nanualh").val(data.interesh2)
										 $("#iperiodicoh").val(data.interesh3)
									break;

								}
							}
						});
					}


				});
			}
			function panelheader()
			{
				$(".panel-slide").slideUp();
				$(".panel-header").on("click",function(e){
					if($(this).attr("data-close")==1)
					{
						$(this).attr("data-close",0);
						$(this).find(".panel-slide").slideUp();

					}else
					{
						$(this).attr("data-close",1);
						$(this).find(".panel-slide").slideDown();
					}
				});
			}
			panelheader();
			function paralelo()
			{
				$(".btn-paralelo").on("click",function(e){
					e.preventDefault();
					$(".contTable-1").html($(".panel-header-1").html());
					$(".contTable-2").html($(".panel-header-2").html());

				})
			}
			paralelo();

			function clearInteres()
			{
				$(".checkbox-per").on("click",function(){
					$(".interes").val("");
					$(".btn-paralelo,.btn-pdf").hide();
				});
			}

			function generatePDF()
			{

				$(".btn-pdf").on("click",function(){
					var nombre     =  $("#nombre").val();
					var cedula     =  $("#cedula").val();
					var prestamo   =  $("#prestamo").val();
					var plazo      =  $("#plazo").val();
					var periodo    =  $(".checkbox-per:checked").val();
					var eanual     =  $("#eanualh").val();
					var nanual     =  $("#nanualh").val();
					var iperiodico =  $("#iperiodicoh").val();
					var uri = 'exemple07.php?nombre='+nombre+"&cedula="+cedula+"&prestamo="+prestamo+"&plazo="+plazo+"&periodo="+periodo+"&eanual="+eanual+"&nanual="+nanual+"&iperiodico="+iperiodico;
					var res = encodeURI(uri);
					window.open("pdf/pdf/"+res, '_blank');
				});
			}
			clearInteres();
			generatePDF();
		});
	</script>
</html>

<div class="modal fade" id="modalparalelo">
	<div class="modal-dialog modal-lg" style="width: 95%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Paralelo</h4>
			</div>
			<div class="modal-body">
				<div class="row cont-table-mod">

					<div class="col-md-5 contTable-1"></div>

					<div class="col-md-7 contTable-2"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
