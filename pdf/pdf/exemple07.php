<?php
/**
 * HTML2PDF Library - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @package   Html2pdf
 * @author    Laurent MINGUET <webmaster@html2pdf.fr>
 * @copyright 2016 Laurent MINGUET
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
    ob_start();
    $periodo=$_GET['periodo'];
    $plazo  =  $_GET['plazo'];
    $ip=$_GET['iperiodico'];
    $nanual=$_GET['nanual'];
    $eanual=$_GET['eanual'];
    $fecha = date('Y-m-d');
    $nperiodo = $plazo * $periodo;
    $prestamo = $_GET['prestamo'];

    $numeroPe="";
    switch ($periodo) {
        case '12':
            $numeroPe=1;
            break;
        case '6':
            $numeroPe=2;
            break;
        case '4':
            $numeroPe=3;
            break;
    }
    $findme   = '.';
    $pos = strpos($ip, $findme);
    if(is_float($ip) || $pos !== false)
    {

        list($enter,$format)=explode(".",$ip);
        $format= str_split($format);
        $formatip=$enter.".".$format[0].$format[1];
    }else{
        $formatip=$ip;
    }
    $findme   = '.';
    $pos = strpos($nanual, $findme);
    if(is_float($nanual) || $pos !== false)
    {

        list($enter,$formatn)=explode(".",$nanual);
        $formatn= str_split($formatn);
        $formatno=$enter.".".$formatn[0].$formatn[1];
    }else
    {
       $formatno = $nanual;
    }
     
        $findme   = '.';
        $pos = strpos($eanual, $findme);
    if(is_float($eanual) || $pos !== false)
    {   
        list($enter,$formatea)=explode(".",$eanual);
        $formatea= str_split($formatea);
        $formatean=$enter.".".$formatea[0].$formatea[1];
    }else{

       $formatean = $eanual;
    }


    function sumarFechas($fecha, $numeroP)
    {
        $nuevafecha = strtotime('+' . $numeroP . ' month', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        return $nuevafecha;
    }
    


      $aCapital = $prestamo / $nperiodo;
      $nperiodo = $plazo * $periodo;

        $ip           = $ip / 100;
        $amortizacion = pow((1 + $ip), ($nperiodo)) * $ip / (pow((1 + $ip), ($nperiodo)) - 1);
        $amortizacion = $prestamo * $amortizacion;
       

    

?> 

<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #272727; border-bottom: solid 1mm #a0a0a0; padding: 2mm; margin-bottom: 150px;}
    table.page_footer {width: 100%; border: none; background-color: #a0a0a0; border-top: solid 1mm #a0a0a0; padding: 2mm}
}
-->
</style>
<page backcolor="#FEFEFE" backimg="" backimgx="center" backimgy="bottom" backimgw="100%" backtop="0" backbottom="10mm" footer="date;heure;page" style="font-size: 12pt">
    <bookmark title="Lettre" level="0" ></bookmark>
    <page_header>
        <table class="page_header">
            <tr>
                <td style="width: 100%; text-align: left; ">
                    <img style="width: 50%;" src="../../img/logoeconommica.png" alt="">
                </td>
            </tr>
        </table>
    </page_header>
    <br>
    <br>
    <br>
    <table  style="width: 370px; margin-top: 40px;  font-size: 12pt; border: 1px solid #000000;">
        <tr>
            <td style="width: 400px;" align="LEFT"><h4>DETALLE PRESTAMO </h4></td>
            <td style="width: 340px">Fecha <?php echo $fecha ?></td>
        </tr>
        <tr>
            <td ><b>Nombre:</b> <?php echo $_GET['nombre'] ?></td>
            <td ><b>Interes Periodico:</b><?php echo $formatip ?>%</td>
        </tr>
        <tr>
            <td ><b>Cedula:</b> <?php echo $_GET['cedula'] ?></td>
            <td ><b>Nominal Anual:</b> <?php echo $formatno ?>%</td>
        </tr>
        <tr>
            <td ><b>Valor Prestamo:</b>$ <?php echo number_format((float) $prestamo)?></td>
            <td ><b>Efectiva Anual:</b> <?php echo $formatean ?>%</td>
        </tr>
        <tr>
            <td ><b>Numero de Cuotas:</b> <?php echo $nperiodo?> Meses</td>
            <td ></td>
        </tr>
        
    </table>
    <br>
    <br>
    <h4>TABLA AMORTIZACIÓN A CAPITAL</h4>
    <table cellspacing="0" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 10pt;">
        <tr>
            <th style="width: 8%">Cuota</th>
            <th style="width: 16%">Fecha</th>
            <th style="width: 22%">Saldo Capital</th>
            <th style="width: 18%">Amort. Capital</th>
            <th style="width: 18%">Interes</th>
            <th style="width: 18%">Flujo de Caja</th>
        </tr>
    </table>
<?php
    $j     = 0;

    for ($i=0; $i<=$nperiodo; $i++) {
        ?>
        <table cellspacing="0" style="width: 100%; border: solid 1px black; background: #F7F7F7; text-align: center; font-size: 10pt;">
        <tr>
        <?php 
        if ($i == 0) {
            ?>
            <td style="width: 8%; text-align: center"><?php echo $i ?></td>
            <td style="width: 16%; text-align: center"><?php echo $fecha; ?></td>
            <td style="width: 22%; text-align: center">$<?php echo number_format((float) $prestamo)?></td>
            <td style="width: 18%">-°-</td>
            <td style="width: 18%; text-align: center;">-°-</td>
            <td style="width: 18%; text-align: center;">$<?php echo number_format((float) $prestamo) ?></td>
            <?php 
        }else{
             if ($j == 0) {
                    $j++;
                    $pagoCapital = $prestamo - $aCapital;
                    $interes     = $prestamo * $ip;
                    $flujoCaja   = $interes + $aCapital;

                } else {
                    $interes     = $pagoCapital * $ip;
                    $flujoCaja   = $interes + $aCapital;
                    $pagoCapital = $pagoCapital - $aCapital;
                    if ($pagoCapital < 0) {
                        $pagoCapital = 0;
                    }

                }


            $fecha=sumarFechas($fecha, $numeroPe);
            ?>
            <td style="width: 8%; text-align: center"><?php echo $i ?></td>
            <td style="width: 16%; text-align: center"><?php echo $fecha ?></td>
            <td style="width: 22%; text-align: center"><?php echo number_format($pagoCapital) ?></td>
            <td style="width: 18%; text-align: center"><?php echo number_format($aCapital) ?></td>
            <td style="width: 18%; text-align: center;"><?php echo number_format($interes, 2) ?></td>
            <td style="width: 18%; text-align: center;"><?php echo number_format($flujoCaja, 2) ?></td>
            <?php             
        }       
?>
        </tr>   
        </table>
<?php
    }
?>

  
    <br>
    <h4>TABLA AMORTIZACIÓN A CUOTA IGUAL DE AMORTIZACIÓN GRADUAL</h4>
    <table cellspacing="0" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 10pt;">
        <tr>
            <th style="width: 8%">Cuota</th>
            <th style="width: 16%">Fecha</th>
            <th style="width: 16%">Saldo Capital</th>
            <th style="width: 15%">Amort. Capital</th>
            <th style="width: 15%">Amort. Interes</th>
            <th style="width: 15%">Cuota Fija</th>
            <th style="width: 15%">Flujo de Caja</th>
        </tr>
    </table>
<?php
    $j     = 0;
    for ($i=0; $i<=$nperiodo; $i++) {
        ?>
        <table cellspacing="0" style="width: 100%; border: solid 1px black; background: #F7F7F7; text-align: center; font-size: 10pt;">
        <tr>
        <?php 
        if ($i == 0) {
            ?>
            <td style="width: 8%; text-align: center"><?php echo $i ?></td>
            <td style="width: 16%; text-align: center"><?php echo $fecha; ?></td>
            <td style="width: 16%; text-align: center">$<?php echo number_format((float) $prestamo)?></td>
            <td style="width: 15%">-°-</td>
            <td style="width: 15%; text-align: center;">-°-</td>
            <td style="width: 15%; text-align: center;">-°-</td>
            <td style="width: 15%; text-align: center;">$<?php echo number_format((float) $prestamo) ?></td>
            <?php 
        }else{
             if ($j == 0) {
                    $j++;
                    $interes       = $prestamo * $ip;
                    $amorCap       = $amortizacion - $interes;
                    $pagoCapital   = $prestamo - $amorCap;

                } else {
                    $interes     = $pagoCapital * $ip;
                    $amorCap     = $amortizacion - $interes;
                    $pagoCapital = $pagoCapital - $amorCap;
                    if ($pagoCapital < 0) {
                        $pagoCapital = 0;
                    }

                }
                if($i==$nperiodo) {
                        $pagoCapitale = 0;
                    }else{
                        $pagoCapitale=number_format($pagoCapital, 2,".",",");
                    }


            $fecha=sumarFechas($fecha, $numeroPe);
            ?>
            <td style="width: 8%; text-align: center"><?php echo $i ?></td>
            <td style="width: 16%; text-align: center"><?php echo $fecha ?></td>
            <td style="width: 16%; text-align: center"><?php echo $pagoCapitale ?></td>
            <td style="width: 15%; text-align: center"><?php echo number_format($amorCap, 2) ?></td>
            <td style="width: 15%; text-align: center;"><?php echo number_format($interes, 2) ?></td>
            <td style="width: 15%; text-align: center;"><?php echo number_format($amortizacion, 2) ?></td>
            <td style="width: 15%; text-align: center;">- <?php echo number_format($amortizacion, 2) ?></td>
            <?php             
        }       
?>
        </tr>   
        </table>
<?php
    }
?>

    <nobreak>
       
    </nobreak>
</page>
<?php 
    $content = ob_get_clean();

    // convert to PDF
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->pdf->SetDisplayMode('fullpage');
//      $html2pdf->pdf->SetProtection(array('print'), 'spipu');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('prestamo.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
