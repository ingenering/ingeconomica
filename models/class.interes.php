<?php
/**
 *
 */
class Interes
{
    private $nperiodo;

    public function __construct($data)
    {

        $this->plazo      = $data['plazo'];
        $this->periodo    = $data['periodo'];
        $this->eAnual     = $data['eanual'];
        $this->nAnual     = $data['nanual'];
        $this->iperiodico = $data['iperiodico'];

    }
    public function efectivaAnual($ip)
    {

        $ip      = $ip / 100;
        $dias    = intval(360 / $this->periodo);
        $totalEa = (pow((1 + $ip), (360 / $dias)) - 1) * 100;
        return $totalEa;

    }
    public function nominalAnual($ip)
    {
        $ip      = $ip / 100;
        $totalEa = ($ip * $this->periodo) * 100;
        return $totalEa;

    }
    public function interesPeriodico($method)
    {
        if ($method == 1) {

            $efectivaAnual = $this->eAnual  / 100;
            $dias          = intval(360 / $this->periodo);
            $ip            = (pow((1 + $efectivaAnual), ($dias / 360)) - 1) * 100;

        } else {
            $ip = ($this->nAnual / $this->periodo);
        }
        return $ip;

    }
    public function numeroPeriodos()
    {
        return $nperiodo = $this->plazo * $this->periodo;
    }
    private function convertPoint($data)
    {
        return str_replace(",", ".", $data);
    }
    public function getPeriodos()
    {
        $numeroPe="";
    switch ($this->periodo) {
        case '12':
            $numeroPe=1;
            break;
        case '6':
            $numeroPe=2;
            break;
        case '4':
            $numeroPe=3;
            break;
    }
    return $numeroPe;
    }

}
