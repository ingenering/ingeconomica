function validateParsley(form) {
    $.listen('parsley:field:error', function(ParsleyField) {
        ParsleyField.$element.closest('.form-group').removeClass('has-success');
        ParsleyField.$element.closest('.form-group').addClass('has-error');
    });
    $.listen('parsley:field:success', function(ParsleyField) {
        ParsleyField.$element.closest('.form-group').removeClass('has-error');
        // ParsleyField.$element.closest('.form-group').addClass('has-success');
        ParsleyField.$element.closest('.form-group').addClass('has-success');
    });
}
function inputNumerico(id) {
    $(id).keyup(function() {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    return true;
}
function limit_char(elemento, max_chars) {
    $(elemento).keyup(function() {
        if ($(this).val().length > max_chars) {
            $(this).val($(this).val().substr(0, max_chars));
        }
    });
}
function formatNumber(input, errorElement, errorText) {
    var num = $(input).val().replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        $(input).val(num);
    } else {
        $(input).val($(input).val().replace(/[^\d\.]*/g, ''));
    }
}
function validateDecimals(input)
{
     $(input).on("keypress keyup blur",function (event) {
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
}