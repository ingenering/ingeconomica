<?php
/**
 *
 */
class Cuotas extends Interes
{

    public function __construct($data)
    {
        parent::__construct($data);
        $periodos          = $this->numeroPeriodos($data);
        $this->prestamo    = $data['prestamo'];
        $this->eAnualh     = $data['eanualh'];
        $this->nAnualh     = $data['nanualh'];
        $this->iperiodicoh = $data['iperiodicoh'];

    }
    public function amortizacionCaptital()
    {

        return $aCapital = $this->prestamo / $this->numeroPeriodos();
    }
    public function tableAmortizacionCapital()
    {
        $j     = 0;
        $fecha = date('Y-m-d');
        $html  = "";
        $ip    = $this->iperiodicoh / 100;
        for ($i = 0; $i <= $this->numeroPeriodos(); $i++) {
            $html .= "<tr>";
            if ($i == 0) {
                $html .= "<td>" . $i . "</td>";
                $html .= "<td>" . $fecha . "</td>";
                $html .= "<td>" . number_format((float) $this->prestamo) . "</td>";
                $html .= "<td>-°-</td>";
                $html .= "<td>-°-</td>";
                $html .= "<td>" . number_format((float) $this->prestamo) . "</td>";
            } else {
                if ($j == 0) {
                    $j++;
                    $pagoCapital = $this->prestamo - $this->amortizacionCaptital();
                    $interes     = $this->prestamo * $ip;
                    $flujoCaja   = $interes + $this->amortizacionCaptital();

                } else {
                    $interes     = $pagoCapital * $ip;
                    $flujoCaja   = $interes + $this->amortizacionCaptital();
                    $pagoCapital = $pagoCapital - $this->amortizacionCaptital();
                    if ($pagoCapital < 0) {
                        $pagoCapital = 0;
                    }

                }
                $fecha = $this->sumarFechas($fecha, $this->getPeriodos());
                $html .= "<td>" . $i . "</td>";
                $html .= "<td>" . $fecha . "</td>";
                $html .= "<td>" . number_format($pagoCapital) . "</td>";
                $html .= "<td>" . number_format($this->amortizacionCaptital()) . "</td>";
                $html .= "<td>" . number_format($interes, 2) . "</td>";
                $html .= "<td>" . number_format($flujoCaja, 2) . "</td>";

            }
            $html .= "</tr>";
        }
        $nominal    = $this->nAnualh;
        $iPeriodico = $this->iperiodicoh;
        $array      = array("data" => $html, "interes1" => $nominal, "interes2" => $iPeriodico, "method" => 1, "amortizacion" => $this->amortizacionCaptital());
        return $array;
    }
    public function tableAmortizacionGradual()
    {
        $j            = 0;
        $pagoCapital  = 0;

        $fecha        = date('Y-m-d');
        $html         = "";
        $ip           = $this->iperiodicoh / 100;
        $amortizacion = $this->amortizacion();
        for ($i = 0; $i <= $this->numeroPeriodos(); $i++) {
            $html .= "<tr>";
            if ($i == 0) {
                $html .= "<td>" . $i . "</td>";
                $html .= "<td>" . $fecha . "</td>";
                $html .= "<td>" . number_format((float) $this->prestamo) . "</td>";
                $html .= "<td>-°-</td>";
                $html .= "<td>-°-</td>";
                $html .= "<td>-°-</td>";
                $html .= "<td>" . number_format((float) $this->prestamo) . "</td>";
            } else {

                if ($j == 0) {
                    $j++;
                    $interes     = $this->prestamo * $ip;
                    $amorCap     = $amortizacion - $interes;
                    $pagoCapital = $this->prestamo - $amorCap;

                } else {
                    $interes     = $pagoCapital * $ip;
                    $amorCap     = $amortizacion - $interes;
                    $pagoCapital = $pagoCapital - $amorCap;

                }
                   
                    if($i==$this->numeroPeriodos()) {
                        $pagoCapitale = 0;
                    }else{
                    	$pagoCapitale=number_format($pagoCapital, 2,".",",");
                    }
               
                $fecha = $this->sumarFechas($fecha, $this->getPeriodos());
                $html .= "<td>" . $i . "</td>";
                $html .= "<td>" . $fecha . "</td>";
                $html .= "<td>" . $pagoCapitale . "</td>";
                $html .= "<td>" . number_format($amorCap, 2) . "</td>";
                $html .= "<td>" . number_format($interes, 2) . "</td>";
                $html .= "<td>" . number_format($amortizacion, 2) . "</td>";
                $html .= "<td> -" . number_format($amortizacion, 2) . "</td>";
            }

        }
        $array = array("data" => $html, "method" => 1);
        return $array;
    }
    public function sumarFechas($fecha, $numeroP)
    {
        $nuevafecha = strtotime('+' . $numeroP . ' month', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        return $nuevafecha;
    }
    public function amortizacion()
    {

        $ip           = $this->iperiodicoh / 100;
        $amortizacion = pow((1 + $ip), ($this->numeroPeriodos())) * $ip / (pow((1 + $ip), ($this->numeroPeriodos())) - 1);
        $amortizacion = $this->prestamo * $amortizacion;
        return $amortizacion;
    }

}
